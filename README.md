# README #

    Do NOT be concerned with the hist_(xxx) tables, 
    they are for ONLY keeping user history of what data 
    was inserted, updated or deleted from each of the 
    normal tables. 
    
    These history tables should NEVER be used by the 
    program source code, they are updated by backend triggers.
---
## Table Relationship Diagram 
Database 20210423 [Image of Model](https://github.com/taylja/images/blob/main/schema20210423_1.png)

Database 20210511 [Image of Model](https://github.com/taylja/images/blob/main/schema20210511.png)

Database Update 20210615 [Image of Model](https://github.com/taylja/images/blob/main/schema20210615.png)

Database 20210623 [Image of Model](https://github.com/taylja/images/blob/main/schema20210623.png)

Database 20210713 [Image of Model](https://github.com/taylja/images/blob/main/schema20210713.png)

Database 20210719 [Image of Model](https://github.com/taylja/images/blob/main/schema20210719.png)

Database 20210726 [Image of Model](https://github.com/taylja/images/blob/main/schema20210726.png)

Database 20210806 [Image of Model](https://github.com/taylja/images/blob/main/schema20210806.png)

---